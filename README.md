# yoshi0518/cakephp:3.8.10

## Author

yoshi0518

## Description

CakePHP development work container

## Version

- Nginx 1.17.8
- PHP 7.4.3
- MySQL 8.0.19
- CakePHP 3.8.11
- phpMyAdmin 5.0.1

## How to Use

### CakePHP

```bash
# Build
$ docker-compose build

# Create and launch container
$ docker-compose up -d

# List containers
$ docker-compose ps

# Connect to mysql container
$ docker-compose exec mysql /bin/bash

# MySQL login
$ mysql -u root -p

# Create a new database
$ create database qanda character set utf8mb4 collate utf8mb4_ja_0900_as_cs_ks;

# Grant permissions
$ grant all on qanda.* to cakephp@'%';

# MySQL logout
$ exit

# Disconnect from container
$ exit

# Login to phpfpm container
$ docker-compose exec phpfpm /bin/ash

# Get composer
$ curl -s https://getcomposer.org/installer | php

# Create a CakePHP project(Specify minor version)
$ php composer.phar create-project --prefer-dist cakephp/app:3.8.* [project name]

# Disconnect from container
$ exit

# Stop container
$ docker-compose down

# Modify settings in app.php and docker-compose.yml

# Create and launch container
$ docker-compose up -d

# List containers
$ docker-compose ps

# Open CakePHP in browser

# Stop container
$ docker-compose down
```

#### Modify settings in app.php

|item|value|
|:----:|:-----:|
|App => defaultTimezone|Asia/Tokyo|
|Datasources => default => host|mysql|
|Datasources => default => username|cakephp|
|Datasources => default => password|password|
|Datasources => default => database|qanda|
|Datasources => default => encoding|utf8mb4|
|Datasources => default => timezone|Asia/Tokyo|

#### Modify settings in docker-compose.yml

|item|value|
|:----:|:-----:|
|services => host => environment => PRJ_NAME|qanda|

### MySQL

```bash
# Check valid my.conf
$ mysql --help | grep my.cnf
$ cat /etc/my.cnf

# MySQL login
$ mysql -u root -p

# Variable check(character code)
$ show variables like '%chara%';

# Variable check(collation)
$ show variables like '%colla%';

# Variable check(default authentication)
$ show variables like '%auth%';

# Change Database
$ use sample

# Execute select
$ select * from sample_table;

# MySQL logout
$ exit
```

## Link

[sample page](http://localhost)

[phpinfo](http://localhost/phpinfo.php)

[phpMyAdmin](http://localhost:8080/)

[CakePHP](http://localhost:8765)
