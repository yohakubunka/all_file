-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- ホスト: cakephp-mysql
-- 生成日時: 2020 年 4 月 22 日 03:32
-- サーバのバージョン： 8.0.19
-- PHP のバージョン: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- データベース: `qanda`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `posts`
--

CREATE TABLE `posts` (
  `id` int UNSIGNED NOT NULL COMMENT '投稿ID',
  `title` text COMMENT '投稿タイトル',
  `content` text COMMENT '投稿内容',
  `user_id` int UNSIGNED DEFAULT NULL COMMENT '投稿したuser-id',
  `kidoku` tinyint(1) DEFAULT NULL COMMENT '既読用フラグ',
  `created` datetime DEFAULT NULL COMMENT '作成日時',
  `modified` datetime DEFAULT NULL COMMENT '最終更新日時'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- テーブルの構造 `replys`
--

CREATE TABLE `replys` (
  `id` int UNSIGNED NOT NULL COMMENT '返信ID',
  `source-id` int UNSIGNED DEFAULT NULL COMMENT '返信先のID',
  `source-type` tinyint(1) DEFAULT NULL COMMENT '返信先の種別',
  `content` text COMMENT '返信内容',
  `created` datetime DEFAULT NULL COMMENT '作成日時',
  `modified` datetime DEFAULT NULL COMMENT '最終更新日時'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- テーブルの構造 `users`
--

CREATE TABLE `users` (
  `id` int UNSIGNED NOT NULL COMMENT 'ユーザ設定ID',
  `username` varchar(256) DEFAULT NULL COMMENT 'ユーザ名',
  `password` varchar(256) DEFAULT NULL COMMENT 'パスワード',
  `name` varchar(64) DEFAULT NULL COMMENT 'ユーザ名前(日本語)',
  `mail` varchar(64) DEFAULT NULL COMMENT 'メールアドレス',
  `created` datetime DEFAULT NULL COMMENT '作成日時',
  `modified` datetime DEFAULT NULL COMMENT '最終更新日時'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- ダンプしたテーブルのインデックス
--

--
-- テーブルのインデックス `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- テーブルのインデックス `replys`
--
ALTER TABLE `replys`
  ADD PRIMARY KEY (`id`);

--
-- テーブルのインデックス `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- ダンプしたテーブルのAUTO_INCREMENT
--

--
-- テーブルのAUTO_INCREMENT `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '投稿ID';

--
-- テーブルのAUTO_INCREMENT `replys`
--
ALTER TABLE `replys`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '返信ID';

--
-- テーブルのAUTO_INCREMENT `users`
--
ALTER TABLE `users`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ユーザ設定ID';
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
